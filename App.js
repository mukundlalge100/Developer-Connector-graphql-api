const express = require("express");
const app = express();
const passport = require("passport");

// DOT ENV SETTING ...
const dotEnv = require("dotenv");
dotEnv.config({ path: "./config/config.env" });

// GRAPHQL IMPORTS ...
const expressGraphQL = require("express-graphql");
const Resolvers = require("./GraphQL/Resolvers/Resolvers");
const GraphQLSchema = require("./GraphQL/Schema/Schema");
const userController = require("./controllers/UserController/UserController");

const bodyParser = require("body-parser");
const path = require("path");
const mongoose = require("mongoose");

const isAuth = require("./Middleware/Auth");

// CORS SETTING ...
app.use((request, response, next) => {
  response.setHeader("Access-Control-Allow-Origin", "*");
  response.setHeader(
    "Access-Control-Allow-Methods",
    "OPTIONS,PUT,POST,GET,PATCH,DELETE"
  );
  response.setHeader(
    "Access-Control-Allow-Headers",
    "Content-Type,Authorization,X-JSON,x-requested-with, X-Auth-Token"
  );
  if (request.method === "OPTIONS") {
    return response.sendStatus(200);
  }
  next();
});

// BODY PARSER MIDDLEWARE ...
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

const port = process.env.PORT || 4000;

app.listen(port, () => {
  console.log(`Server is running on port ${port}`);
});

// DB CONNECTION ...
mongoose
  .connect(process.env.MONGODB_URI, {
    useNewUrlParser: true
  })
  .then(() => console.log("MongoDB Connected!"))
  .catch(error => console.error(error));

// ISAUTH CONFIG ...
require("./config/passport")(passport);
app.use(isAuth);

// GRAPHQL CONFIGURATION ...
app.use(
  "/graphql",
  expressGraphQL({
    graphiql: true,
    schema: GraphQLSchema,
    rootValue: Resolvers,
    customFormatErrorFn(error) {
      if (!error.originalError) {
        return error;
      }
      const errors = error.originalError.errors;
      const message = error.message;
      const status = error.originalError.status;
      return { errors, message, status };
    },
    pretty: true
  })
);
// SERVER STATIC ASSETS IF IN PRODUCTION ...
if (process.env.NODE_ENV === "production") {
  // SET STATIC FOLDER ...
  app.use(express.static(path.join(__dirname, "client", "build")));
  app.get("*", (request, response) => {
    response.sendFile(path.join(__dirname, "client", "build", "index.html"));
  });
}
