const userController = require("../../controllers/UserController/UserController");
const profileController = require("../../controllers/ProfileController/ProfileController");
const postController = require("../../controllers/PostController/PostController");

module.exports = {
  // MUTATIONS RESOLVERS ...

  // USER MUTATION ...
  createUser: (_, request) => userController.postSignUp(request),

  //PROFILE MUTATION ...
  createProfile: (_, request) => profileController.postProfile(request),
  createExperience: request => profileController.postExperience(request),
  deleteExperience: (_, request) => profileController.deleteExperience(request),
  createEducation: (_, request) => profileController.postEducation(request),
  deleteEducation: (_, request) => profileController.deleteEducation(request),
  deleteProfileAndAccount: (_, request) =>
    profileController.deleteProfileAndAccount(request),

  // POST MUTATION ...
  createPost: (_, request) => postController.createPost(request),
  deletePost: (_, request) => postController.deletePost(request),
  likePostById: (_, request) => postController.likePostById(request),
  unlikePostById: (_, request) => postController.unlikePostById(request),
  createComment: (_, request) => postController.postComment(request),
  deleteComment: (_, request) => postController.deleteComment(request),

  // QUERY RESOLVERS ...
  validateEmail: (_, request) => userController.validateEmail(request),

  postNewPassword: (_, request) => userController.postNewPassword(request),
  postResetPassword: (_, request) => userController.postResetPassword(request),

  logIn: (_, request, args3, args4) => userController.postLogIn(request),
  getProfile: (_, request) => profileController.getProfile(request),
  getProfileByHandle: (_, request) =>
    profileController.getProfileByHandle(request),
  getProfileByUserId: (_, request) =>
    profileController.getProfileByUserId(request),
  getProfiles: (_, request) => profileController.getProfiles(request),
  getPosts: (_, request) => postController.getPosts(request),
  getPost: (_, request) => postController.getPost(request)

  // SUBSCRIPTION RESOLVERS ...
};
