const { buildSchema } = require("graphql");

module.exports = buildSchema(`
  
  type User {
    _id: ID!
    userName: String
    email: String
    password: String
    avatar: String
    date: String 
  }
  type Profile {
    _id:ID!
    user:User!
    handle:String!
    company:String
    website:String 
    location:String
    status :String!
    skills:[String!]!
    bio:String
    githubUserName:String
    experience:[Experience]
    education:[Education]
    social:SocialLinks
    date:String
  }
  type Post {
    _id:ID!
    user:User
    text:String! 
    userName:String
    avatar:String
    likes:[Like!]
    disLikes:[DisLike!]
    comments:[Comment!]
    date:String
  }
  type Like{
    _id:ID!
    user:User!
  }
  type DisLike{
    _id:ID!
    user:User!
  }
  type Comment {
    _id:ID!
    user:User!
    text:String! 
    userName:String
    avatar:String
    date:String
  }
  type SocialLinks {
    youtube:String
    facebook:String
    linkedin:String 
    twitter:String 
    instagram:String
  }
  type Education {
    _id:ID!
    school:String! 
    degree:String! 
    fieldOfStudy:String! 
    from:String! 
    to:String
    current:Boolean 
    description:String 
  }

  type Experience {
    _id:ID!
    title:String!
    company:String! 
    location:String 
    from:String! 
    to:String 
    current:Boolean
    description:String! 
  }
  
  type Query {
    logIn:AuthData!
    validateEmail:CommonResponse!
    getProfile:Profile!
    getProfileByHandle:Profile!
    getProfileByUserId:Profile!
    getProfiles:[Profile!]!
    getPost:Post!
    getPosts:[Post!]!
  }

  type AuthData {
    token:String!
  }
  
  
  
  type Mutation {
    createUser: CommonResponse!
    postNewPassword:CommonResponse!
    postResetPassword:CommonResponse!
    createExperience:Profile!
    createEducation:Profile!
    createProfile:Profile!
    deleteProfileAndAccount:DeleteProfileAndAccountResponse!
    deleteExperience:Profile!
    deleteEducation:Profile!
    createPost:Post!
    deletePost:CommonResponse!
    likePostById:Post!
    unlikePostById:Post!
    createComment:Post!
    deleteComment:Post!
  }
  type CommonResponse {
    success:Boolean
    message:String
  }

  type DeleteProfileAndAccountResponse {
    message:String!
  }
  schema {
    mutation:Mutation
    query:Query
  }
`);
