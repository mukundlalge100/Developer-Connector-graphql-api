import React from "react";
import classes from "../Input.module.scss";

// SELECT INPUT FUNCTIONAL COMPONENT...
const SelectField = fieldProps => {
  // SELECT OPTIONS FOR SELECT ELEMENT ...

  const selectOptions = fieldProps.options.map(option => {
    return (
      <option value={option.value} key={option.label}>
        {option.label}
      </option>
    );
  });
  return (
    <div className={`${classes.Input}`}>
      <select
        className={`${classes.Input__InputElement}   ${
          fieldProps.error || (fieldProps.meta.error && fieldProps.meta.touched)
            ? classes.Input__Invalid
            : ""
        }`}
        placeholder={fieldProps.placeholder}
        {...fieldProps.input}
      >
        {selectOptions}
      </select>

      <label className={classes.Input__Label}>{fieldProps.label}</label>

      {fieldProps.info ? (
        <small className={classes.Input__Text}>{fieldProps.info}</small>
      ) : null}

      {fieldProps.error || fieldProps.meta.touched ? (
        <p className={classes.Input__IsInvalid}>
          {fieldProps.error || fieldProps.meta.error}
        </p>
      ) : null}
    </div>
  );
};
export default SelectField;
