import React from "react";
import classes from "../Input.module.scss";

// TEXTAREA INPUT FUNCTIONAL COMPONENT...
const TextAreaField = fieldProps => {
  return (
    <div className={`${classes.Input}`}>
      <textarea
        className={`${classes.Input__InputElement}   ${
          fieldProps.error || (fieldProps.meta.error && fieldProps.meta.touched)
            ? classes.Input__Invalid
            : ""
        }`}
        placeholder={fieldProps.placeholder}
        {...fieldProps.input}
      />
      <label className={classes.Input__Label}>{fieldProps.label}</label>

      {fieldProps.info ? (
        <small className={classes.Input__Text}>{fieldProps.info}</small>
      ) : null}

      {fieldProps.error || fieldProps.meta.touched ? (
        <p className={classes.Input__IsInvalid}>
          {fieldProps.error || fieldProps.meta.error}
        </p>
      ) : null}
    </div>
  );
};

export default TextAreaField;
