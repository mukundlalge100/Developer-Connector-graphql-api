import React from "react";
import classes from "./PostComment.module.scss";
import utilClasses from "../../Util/Util.module.scss";
import { ReactComponent as Bin } from "../../Assets/SVG/close.svg";
import PropTypes from "prop-types";
import { Link } from "react-router-dom";

const PostComment = props => {
  let postCommentItems;
  if (props.post.comments.length > 0) {
    postCommentItems = props.post.comments.map(comment => (
      <div className={classes.PostComment_Items} key={comment._id}>
        <Link
          className={classes.PostComment_Items__User}
          to={`/profile/user/${comment.user._id}`}
        >
          <img
            src={comment.avatar}
            alt="User Avatar"
            className={classes.PostComment_Items__User_Image}
          />
          <span className={classes.PostComment_Items__User_Name}>
            {comment.userName}
          </span>
        </Link>
        <div className={classes.PostComment_Items__Text}>
          <p className={utilClasses.Paragraph}>{comment.text}</p>
          <span style={{ fontSize: "1.1rem" }}>
            {new Date(parseInt(comment.date)).toDateString()}
          </span>
          {props.user._id === comment.user._id ? (
            <Bin
              className={classes.PostComment_Items__Text_Icon}
              onClick={() => props.onDeleteComment(comment._id)}
            />
          ) : null}
        </div>
      </div>
    ));
  }
  return (
    <div className={classes.PostComment}>
      {props.post.comments.length > 0 ? (
        <h2 className={utilClasses.Secondary__Heading}>Comments...</h2>
      ) : null}
      {postCommentItems}
    </div>
  );
};
PostComment.propTypes = {
  post: PropTypes.object.isRequired,
  user: PropTypes.object.isRequired,
  onDeleteComment: PropTypes.func.isRequired
};

export default PostComment;
