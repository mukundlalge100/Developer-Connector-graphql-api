// const validator = require("validator");
import { isEmpty } from "../../../Util/Util";

const validateEducationInput = data => {
  let errors = {};
  if (isEmpty(data.school)) {
    errors.school = "School field is required!";
  }
  if (isEmpty(data.degree)) {
    errors.degree = "Degree field is required!";
  }
  if (isEmpty(data.fieldOfStudy)) {
    errors.fieldOfStudy = "FieldOfStudy field is required!";
  }
  if (isEmpty(data.from)) {
    errors.from = "From date field is required!";
  }
  return errors;
};

export default validateEducationInput;
