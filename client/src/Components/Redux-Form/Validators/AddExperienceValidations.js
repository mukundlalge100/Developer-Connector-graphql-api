// import validator from "validator";
import { isEmpty } from "../../../Util/Util";

const validateExperienceInput = data => {
  let errors = {};
  if (isEmpty(data.title)) {
    errors.title = "Title field is required!";
  }
  if (isEmpty(data.company)) {
    errors.company = "Company field is required!";
  }
  if (isEmpty(data.from)) {
    errors.from = "From date field is required!";
  }
  return errors;
};

export default validateExperienceInput;
