import validator from "validator";
import { isEmpty } from "../../../Util/Util";

const validateAddPostInput = data => {
  let errors = {};

  if (isEmpty(data.text)) {
    errors.text = "Text Field is required!";
  } else if (!validator.isLength(data.text, { min: 10, max: 500 })) {
    errors.text = "Post must be between 10 to 500 characters!!";
  }
  return errors;
};

export default validateAddPostInput;
