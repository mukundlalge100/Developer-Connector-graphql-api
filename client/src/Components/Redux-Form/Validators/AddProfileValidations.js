import validator from "validator";

import { isEmpty } from "../../../Util/Util";

const validateProfileInput = ({
  handle,
  status,
  skills,
  website,
  facebook,
  twitter,
  linkedin,
  instagram,
  youtube
}) => {
  let errors = {};
  if (isEmpty(handle)) {
    errors.handle = "Handle field is required!";
  } else if (!validator.isLength(handle, { min: 4, max: 30 })) {
    errors.handle = "Handle must be in between 4 and 30 characters!";
  }

  if (isEmpty(status)) {
    errors.status = "Status field is required!";
  }

  if (isEmpty(skills)) {
    errors.skills = "Skills field is required!";
  }

  if (!isEmpty(website)) {
    if (!validator.isURL(website)) {
      errors.website = "Website has not valid URL";
    }
  }
  if (!isEmpty(facebook)) {
    if (!validator.isURL(facebook)) {
      errors.facebook = "Facebook has not valid URL";
    }
  }
  if (!isEmpty(twitter)) {
    if (!validator.isURL(twitter)) {
      errors.twitter = "Twitter has not valid URL";
    }
  }
  if (!isEmpty(linkedin)) {
    if (!validator.isURL(linkedin)) {
      errors.linkedin = "Linkedin has not valid URL";
    }
  }
  if (!isEmpty(youtube)) {
    if (!validator.isURL(youtube)) {
      errors.youtube = "Youtube has not valid URL";
    }
  }
  if (!isEmpty(instagram)) {
    if (!validator.isURL(instagram)) {
      errors.instagram = "Instagram has not valid URL";
    }
  }
  return errors;
};
export default validateProfileInput;
