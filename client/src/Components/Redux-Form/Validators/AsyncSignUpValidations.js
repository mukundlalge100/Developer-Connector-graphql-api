import axios from "../../../api/DevConnector";
import GraphQLQueries from "../../../GraphQL/UserQueries";

const asyncSignUpValidations = async (
  validateEmailData,
  dispatch,
  props,
  field
) => {
  const errors = {};
  try {
    const response = await axios.post("/graphql", {
      query: GraphQLQueries.validateEmailQuery,
      validateEmailData
    });
    if (!response.data.data.validateEmail.success) {
      errors.email = response.data.data.validateEmail.message;
    } else {
      return;
    }
  } catch (error) {
    if (error) {
      console.log(error);
    }
  }
  throw errors;
};
export default asyncSignUpValidations;
