import React, { Component } from "react";
import classes from "./LogIn.module.scss";
import utilClasses from "../../../Util/Util.module.scss";
import { connect } from "react-redux";
import * as actions from "../../../Store/Actions/IndexAction";
import Loader from "../../../Components/UI/Loader/Loader";
import logInValidations from "../../../Components/Redux-Form/Validators/LogInValidations";

import SomethingWentWrong from "../../../HOC/ErrorHandler/SomethingWentWrong";
import { Link } from "react-router-dom";
import { Form, Field, reduxForm } from "redux-form";
import RenderInput from "../../../Components/Redux-Form/Renderers/RenderInput";
// LOGIN CLASS BASED COMPONENT ...
class LogIn extends Component {
  state = {
    logInForm: {
      email: "",
      password: ""
    },
    showHidePassword: false
  };

  componentDidMount = () => {
    // CHECK IF USER IS AUTHENTICATED OR NOT IF YES REDIRECT USER TO DASHBOARD FROM LOGIN...
    if (this.props.isAuthenticated) {
      this.props.history.push("/dashboard");
    }
  };
  componentDidUpdate = () => {
    if (this.state.showHidePassword) {
      setTimeout(() => {
        this.setState({ showHidePassword: false });
      }, 1000);
    }
  };
  somethingWentWrongCloseHandler = () => {
    this.props.onSomethingWentWrongClose();
  };
  //LOGIN FORM SUBMIT HANDLER METHOD ...
  logInFormSubmitHandler = formValues => {
    const userData = {
      email: formValues.email,
      password: formValues.password
    };
    // CALLING LOGIN METHOD IN REDUX STORE...
    this.props.onAuthLogIn(userData, this.props.history);
  };

  // SHOW OR HIDE PASSWORD FOR PASSWORD FIELD SIMPLE DOM METHOD ...
  showHidePassword = () => {
    let passwordElement = document.getElementById("password");
    if (passwordElement.type === "password") {
      passwordElement.type = "text";
      setTimeout(() => {
        passwordElement.type = "password";
      }, 1000);
    }
    this.setState(prevState => {
      return { showHidePassword: !prevState.showHidePassword };
    });
  };
  render() {
    let errors = { ...this.props.authLogInFormErrors };

    if (this.props.authLogInFormLoading) {
      // RENDERING LOADER ON SCREEN WHEN LOGIN FORM IS SUBMITED UNTIL SUCCESS...
      return (
        <main className={classes.LogIn}>
          <Loader />
        </main>
      );
    }
    if (this.props.somethingWentWrong) {
      return (
        <SomethingWentWrong
          somethingWentWrong={this.props.somethingWentWrong}
          somethingWentWrongCloseHandler={this.somethingWentWrongCloseHandler}
        />
      );
    }
    return (
      <main className={classes.LogIn}>
        <div className={classes.LogIn_Container}>
          <div className={classes.LogIn_Header}>
            <h1 className={`${utilClasses.Primary__Heading}`}>Log In</h1>
            <p className={`${utilClasses.Paragraph}`}>
              Sign in to your DevConnector account
            </p>
          </div>
          <Form
            onSubmit={this.props.handleSubmit(this.logInFormSubmitHandler)}
            className={classes.LogIn_Form}
          >
            <Field
              name="email"
              component={RenderInput}
              label="Enter Your Email"
              placeholder="Enter Your Email"
              type="text"
              id="email"
              info="Email address should contain '@' character."
              error={errors.emailIsNotValid || errors.emailIsNotExist}
            />
            <Field
              type="password"
              component={RenderInput}
              id="password"
              placeholder="Enter Your Password"
              name="password"
              label="Enter Your Password"
              error={errors.passwordIsNotValid}
              showHidePassword={this.state.showHidePassword}
              showHidePasswordFunc={this.showHidePassword}
            />
            <Link to="/reset-password" className={classes.LogIn_LogInFormLink}>
              Forgot Password?
            </Link>

            <button
              type="submit"
              className={`${utilClasses.Button} ${classes.LogIn_Button}`}
            >
              LogIn
            </button>
          </Form>
        </div>
        <p className={`${utilClasses.Paragraph}`}>
          Don't have an account ?
          <Link to="/signup" className={classes.LogIn_LogInFormLink}>
            {" "}
            Create account here.
          </Link>
        </p>
      </main>
    );
  }
}
const mapStateToProps = state => {
  return {
    authLogInFormLoading: state.authReducer.authLogInFormLoading,
    somethingWentWrong: state.authReducer.somethingWentWrong,
    authLogInFormErrors: state.authReducer.authLogInFormErrors,
    isAuthenticated: state.authReducer.isAuthenticated
  };
};
const mapDispatchToProps = dispatch => {
  return {
    onAuthLogIn: (userData, history) =>
      dispatch(actions.authLogIn(userData, history)),
    onSomethingWentWrongClose: () =>
      dispatch(actions.authSomethingWentWrongCloseHandler())
  };
};
LogIn = connect(
  mapStateToProps,
  mapDispatchToProps
)(LogIn);

export default reduxForm({ form: "logInForm", validate: logInValidations })(
  LogIn
);
