import React, { Component } from "react";
import classes from "./SignUp.module.scss";
import utilClasses from "../../../Util/Util.module.scss";
import { connect } from "react-redux";
import { reduxForm, Field, Form } from "redux-form";
import RenderInput from "../../../Components/Redux-Form/Renderers/RenderInput";

import * as actions from "../../../Store/Actions/IndexAction";
import Loader from "../../../Components/UI/Loader/Loader";
import SomethingWentWrong from "../../../HOC/ErrorHandler/SomethingWentWrong";
import signUpValidations from "../../../Components/Redux-Form/Validators/SignUpValidations";
import asyncSignUpValidations from "../../../Components/Redux-Form/Validators/AsyncSignUpValidations";

// SIGNUP CLASS BASED COMPONENT...
class SignUp extends Component {
  state = {
    signUpForm: {
      userName: "",
      email: "",
      password: "",
      confirmPassword: ""
    },
    showHidePassword: false,
    showHideConfirmPassword: false
  };
  componentDidUpdate = () => {
    if (this.state.showHidePassword) {
      setTimeout(() => {
        this.setState({ showHidePassword: false });
      }, 1000);
    }
    if (this.state.showHideConfirmPassword) {
      setTimeout(() => {
        this.setState({ showHideConfirmPassword: false });
      }, 1000);
    }
  };

  // SHOW OR HIDE PASSWORD FOR PASSWORD FIELD SIMPLE DOM METHOD ...
  showHidePassword = () => {
    let passwordElement = document.getElementById("password");
    if (passwordElement.type === "password") {
      passwordElement.type = "text";
      setTimeout(() => {
        passwordElement.type = "password";
      }, 1000);
    }
    this.setState(prevState => {
      return { showHidePassword: !prevState.showHidePassword };
    });
  };

  // SHOW OR HIDE PASSWORD FOR PASSWORD FIELD SIMPLE DOM METHOD ...
  showHideConfirmPassword = () => {
    let passwordElement = document.getElementById("confirmPassword");
    if (passwordElement.type === "password") {
      passwordElement.type = "text";
      setTimeout(() => {
        passwordElement.type = "password";
      }, 1000);
    } else {
      passwordElement.type = "password";
    }
    this.setState(prevState => {
      return { showHideConfirmPassword: !prevState.showHideConfirmPassword };
    });
  };
  somethingWentWrongCloseHandler = () => {
    this.props.onSomethingWentWrongClose();
  };

  //SIGNUP FORM SUBMIT HANDLER METHOD ...
  signUpFormSubmitHandler = formValues => {
    const userData = {
      userName: formValues.userName,
      email: formValues.email,
      mobileNumber: formValues.mobileNumber,
      password: formValues.password,
      confirmPassword: formValues.confirmPassword
    };

    // CALLING LOGIN METHOD IN REDUX STORE...
    this.props.onAuthSignUp(userData, this.props.history);
  };
  render() {
    const errors = { ...this.props.authSignUpFormErrors };

    if (this.props.authSignUpFormLoading) {
      return (
        // RENDERING LOADER ON SCREEN WHEN SIGN FORM IS SUBMITED UNTIL SUCCESS...
        <main className={classes.SignUp}>
          <Loader />
        </main>
      );
    }
    if (this.props.somethingWentWrong) {
      return (
        <SomethingWentWrong
          somethingWentWrong={this.props.somethingWentWrong}
          somethingWentWrongCloseHandler={this.somethingWentWrongCloseHandler}
        />
      );
    }
    return (
      <main className={classes.SignUp}>
        <div className={classes.SignUp_Container}>
          <div className={classes.SignUp_Header}>
            <h1 className={`${utilClasses.Primary__Heading}`}>Sign Up</h1>
            <p className={`${utilClasses.Paragraph}`}>
              Create your DevConnector account
            </p>
          </div>
          <Form
            onSubmit={this.props.handleSubmit(this.signUpFormSubmitHandler)}
            className={classes.SignUp_Form}
          >
            <Field
              name="userName"
              component={RenderInput}
              info="User name must be in between 2 and 30 character."
              id="name"
              label="User Name"
              error={errors.userNameIsNotValid}
              placeholder="User Name"
            />
            <Field
              component={RenderInput}
              type="email"
              name="email"
              id="email"
              label="Email Address"
              placeholder="Email Address"
              info="This site uses Gravatar so if you want a profile image, use a Gravatar email"
              error={errors.emailIsNotValid || errors.emailIsAlreadyExist}
            />
            <Field
              component={RenderInput}
              type="password"
              name="password"
              info="Password must be in between 6 and 30 characters!"
              label="Password"
              id="password"
              error={errors.passwordIsNotValid}
              placeholder="Password"
              showHidePassword={this.state.showHidePassword}
              showHidePasswordFunc={this.showHidePassword}
            />
            <Field
              component={RenderInput}
              type="password"
              name="confirmPassword"
              id="confirmPassword"
              label="Confirm Password"
              error={errors.confirmPasswordIsNotValid}
              placeholder="Confirm Password"
              showHideConfirmPassword={this.state.showHideConfirmPassword}
              showHideConfirmPasswordFunc={this.showHideConfirmPassword}
            />
            <button
              type="submit"
              className={`${utilClasses.Button} ${classes.SignUp_Button}`}
            >
              SignUp
            </button>
          </Form>
        </div>
      </main>
    );
  }
}

const mapStateToProps = state => {
  return {
    authSignUpFormLoading: state.authReducer.authSignUpFormLoading,
    authSignUpFormErrors: state.authReducer.authSignUpFormErrors,
    somethingWentWrong: state.authReducer.somethingWentWrong
  };
};
const mapDispatchToProps = dispatch => {
  return {
    onAuthSignUp: (userData, history) =>
      dispatch(actions.authSignUp(userData, history)),
    onSomethingWentWrongClose: () =>
      dispatch(actions.authSomethingWentWrongCloseHandler())
  };
};
SignUp = connect(
  mapStateToProps,
  mapDispatchToProps
)(SignUp);

export default reduxForm({
  form: "signUpForm",
  validate: signUpValidations,
  asyncValidate: asyncSignUpValidations,
  asyncChangeFields: ["email"]
})(SignUp);
