import React, { Component } from "react";
import utilClasses from "../../../Util/Util.module.scss";

import classes from "./Add-Comment.module.scss";
import { connect } from "react-redux";
import * as actions from "../../../Store/Actions/IndexAction";
import TextAreaField from "../../../Components/Input/TextAreaField/TextAreaField";
import PropTypes from "prop-types";
import { reduxForm, Field, Form } from "redux-form";
import validateAddPostInput from "../../../Components/Redux-Form/Validators/AddPostValidations";

class AddComment extends Component {
  somethingWentWrongCloseHandler = () => {
    // CALLED WHEN WE CLICK ON BACKDROP OF MODAL COMPONENT ...
    this.props.onSomethingWentWrongClose();
  };
  commentFormSubmitHandler = formValues => {
    const commentFormData = {
      text: formValues.text,
      userName: this.props.user.userName,
      avatar: this.props.user.avatar
    };
    this.props.onAddComment(this.props.postId, commentFormData);
  };
  render() {
    return (
      <div className={classes.AddComment}>
        <h2
          style={{ marginLeft: "1rem" }}
          className={utilClasses.Secondary__Heading}
        >
          Make a Comment ...
        </h2>
        <Form
          className={classes.AddComment_Form}
          onSubmit={this.props.handleSubmit(this.commentFormSubmitHandler)}
        >
          <Field
            component={TextAreaField}
            name="text"
            label="Reply to post"
            placeholder="Reply to post"
            error={this.props.postErrors.textIsNotValid}
          />
          <button
            type="submit"
            className={`${utilClasses.Button} ${classes.AddComment_Form__Button}`}
          >
            Comment
          </button>
        </Form>
      </div>
    );
  }
}
const mapStateToProps = state => {
  return {
    user: state.authReducer.user,
    postErrors: state.postReducer.postErrors,
    postLoading: state.postReducer.postLoading,
    somethingWentWrong: state.postReducer.somethingWentWrong
  };
};
const mapDispatchToProps = dispatch => {
  return {
    onAddComment: (postId, commentData) =>
      dispatch(actions.addComment(postId, commentData))
  };
};

AddComment.propTypes = {
  postId: PropTypes.string.isRequired
};

AddComment = connect(
  mapStateToProps,
  mapDispatchToProps
)(AddComment);

export default reduxForm({
  form: "addCommentForm",
  validate: validateAddPostInput
})(AddComment);
