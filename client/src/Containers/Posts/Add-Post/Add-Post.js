import React, { Component } from "react";
import utilClasses from "../../../Util/Util.module.scss";

import classes from "./Add-Post.module.scss";
import { connect } from "react-redux";
import * as actions from "../../../Store/Actions/IndexAction";
import TextAreaField from "../../../Components/Input/TextAreaField/TextAreaField";
import { reduxForm, Field, Form } from "redux-form";
import validateAddPostInput from "../../../Components/Redux-Form/Validators/AddPostValidations";

class AddPost extends Component {
  somethingWentWrongCloseHandler = () => {
    // CALLED WHEN WE CLICK ON BACKDROP OF MODAL COMPONENT ...
    this.props.onSomethingWentWrongClose();
  };
  postFormSubmitHandler = formValues => {
    const postFormData = {
      text: formValues.text,
      userName: this.props.user.userName,
      avatar: this.props.user.avatar
    };
    this.props.onAddPost(postFormData);
  };
  render() {
    const errors = { ...this.props.postErrors };
    return (
      <div className={classes.AddPost}>
        <h2
          style={{ marginLeft: "1rem" }}
          className={utilClasses.Secondary__Heading}
        >
          Write Something ...
        </h2>
        <Form
          className={classes.AddPost_Form}
          onSubmit={this.props.handleSubmit(this.postFormSubmitHandler)}
        >
          <Field
            component={TextAreaField}
            name="text"
            label="Create a post"
            placeholder="Create a post"
            error={errors.textIsNotValid}
          />
          <button
            type="submit"
            className={`${utilClasses.Button} ${classes.AddPost_Form__Button}`}
          >
            Add Post
          </button>
        </Form>
      </div>
    );
  }
}
const mapStateToProps = state => {
  return {
    user: state.authReducer.user,
    postErrors: state.postReducer.postErrors,
    postLoading: state.postReducer.postLoading,
    somethingWentWrong: state.postReducer.somethingWentWrong
  };
};
const mapDispatchToProps = dispatch => {
  return {
    onAddPost: postData => dispatch(actions.addPost(postData))
  };
};
AddPost = connect(
  mapStateToProps,
  mapDispatchToProps
)(AddPost);

export default reduxForm({
  form: "addPostForm",
  validate: validateAddPostInput
})(AddPost);
