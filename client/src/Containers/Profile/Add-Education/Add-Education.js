import React, { Component } from "react";
import { connect } from "react-redux";
import SomethingWentWrong from "../../../HOC/ErrorHandler/SomethingWentWrong";
import classes from "../Profile.module.scss";
import utilClasses from "../../../Util/Util.module.scss";
import Loader from "../../../Components/UI/Loader/Loader";
import * as actions from "../../../Store/Actions/IndexAction";
import { Link } from "react-router-dom";
import TextAreaField from "../../../Components/Input/TextAreaField/TextAreaField";
import renderInput from "../../../Components/Redux-Form/Renderers/RenderInput";
import { reduxForm, Form, Field } from "redux-form";
import validateEducationInput from "../../../Components/Redux-Form/Validators/AddEducationValidations";
// ADD EDUCATION FORM COMPONENT ...
class AddEducation extends Component {
  state = {
    current: false,
    disabled: false
  };

  somethingWentWrongCloseHandler = () => {
    // CALLED WHEN WE CLICK ON BACKDROP OF MODAL COMPONENT ...
    this.props.onSomethingWentWrongClose();
  };

  checkBoxChangeHandler = () => {
    let opCurrent = !this.state.current;
    let opDisabled = !this.state.disabled;
    this.setState({ current: opCurrent, disabled: opDisabled });
  };

  educationFormSubmitHandler = formValues => {
    this.props.onAddEducation(formValues, this.props.history);
  };

  render() {
    let errors = { ...this.props.profileErrors };

    if (this.props.profileLoading) {
      return (
        <main className={utilClasses.Loader__Centered}>
          <Loader />
        </main>
      );
    }
    if (this.props.somethingWentWrong) {
      return (
        <SomethingWentWrong
          somethingWentWrong={this.props.somethingWentWrong}
          somethingWentWrongCloseHandler={this.somethingWentWrongCloseHandler}
        />
      );
    }
    return (
      <main className={classes.Profile}>
        <div className={classes.Profile_Container}>
          <Link
            to="/"
            style={{
              justifySelf: "start",
              marginTop: "2rem",
              marginLeft: "2rem"
            }}
            className={`${utilClasses.Link}`}
          >
            &larr; Go back
          </Link>
          <header className={classes.Profile_Header}>
            <h1 className={utilClasses.Secondary__Heading}>Add Education</h1>
            <p
              className={utilClasses.Paragraph}
              style={{ justifySelf: "start", marginLeft: "1.5rem" }}
            >
              <br />* fileds are mark as required.
            </p>
          </header>
          <Form
            className={classes.Profile_Form}
            onSubmit={this.props.handleSubmit(this.educationFormSubmitHandler)}
          >
            <Field
              component={renderInput}
              placeholder="* School"
              name="school"
              error={errors.schoolIsNotValid}
              label="* School"
            />
            <Field
              component={renderInput}
              placeholder="* Degree"
              label="* Degree"
              name="degree"
              error={errors.degreeIsNotValid}
            />
            <Field
              component={renderInput}
              placeholder="* Field of study "
              name="fieldOfStudy"
              error={errors.fieldOfStudyIsNotValid}
              label="* Field of study"
            />
            <Field
              component={renderInput}
              name="from"
              type="date"
              error={errors.fromIsNotValid}
              label="* From Date"
            />
            <Field
              component={renderInput}
              type="date"
              name="to"
              label="To Date"
              disabled={this.state.disabled ? true : false}
            />
            <Field
              component={renderInput}
              type="checkbox"
              name="current"
              label="Current Education"
              onChange={this.checkBoxChangeHandler}
              checked={this.state.current}
            />
            <Field
              placeholder="Education Description"
              name="description"
              component={TextAreaField}
              label="Education Description"
            />
            <button
              className={`${utilClasses.Button} ${classes.Profile_Form__Button}`}
              type="submit"
              disabled={this.props.pristine || this.props.submitting}
            >
              Submit Experience
            </button>
          </Form>
        </div>
      </main>
    );
  }
}

const mapStateToProps = state => {
  return {
    profileLoading: state.profileReducer.profileLoading,
    profileErrors: state.profileReducer.profileErrors,
    somethingWentWrong: state.profileReducer.somethingWentWrong
  };
};

const mapDispatchToProps = dispatch => {
  return {
    onAddEducation: (educationFormData, history) =>
      dispatch(actions.addEducation(educationFormData, history)),
    onSomethingWentWrongClose: () =>
      dispatch(actions.profileSomethingWentWrongCloseHandler())
  };
};

AddEducation = connect(
  mapStateToProps,
  mapDispatchToProps
)(AddEducation);

export default reduxForm({
  form: "AddEducationForm",
  validate: validateEducationInput
})(AddEducation);
