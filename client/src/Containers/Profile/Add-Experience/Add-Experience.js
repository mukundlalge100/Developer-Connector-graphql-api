import React, { Component } from "react";
import { connect } from "react-redux";
import classes from "../Profile.module.scss";
import SomethingWentWrong from "../../../HOC/ErrorHandler/SomethingWentWrong";
import utilClasses from "../../../Util/Util.module.scss";
import Loader from "../../../Components/UI/Loader/Loader";
import * as actions from "../../../Store/Actions/IndexAction";
import { Link } from "react-router-dom";
import { reduxForm, Form, Field } from "redux-form";
import renderInput from "../../../Components/Redux-Form/Renderers/RenderInput";
import validateExperienceInput from "../../../Components/Redux-Form/Validators/AddExperienceValidations";
import TextAreaField from "../../../Components/Input/TextAreaField/TextAreaField";

// ADD EXPERIENCE FORM COMPONENT ...
class AddExperience extends Component {
  state = {
    current: false,
    disabled: false
  };
  somethingWentWrongCloseHandler = () => {
    this.props.onSomethingWentWrongClose();
  };
  checkBoxChangeHandler = () => {
    let opCurrent = !this.state.current;
    let opDisabled = !this.state.disabled;
    this.setState({ current: opCurrent, disabled: opDisabled });
  };
  experienceFormSubmitHandler = formValues => {
    this.props.onAddExperience(formValues, this.props.history);
  };
  render() {
    let errors = { ...this.props.profileErrors };

    if (this.props.profileLoading) {
      return (
        <main className={utilClasses.Loader__Centered}>
          <Loader />
        </main>
      );
    }
    if (this.props.somethingWentWrong) {
      return (
        <SomethingWentWrong
          somethingWentWrong={this.props.somethingWentWrong}
          somethingWentWrongCloseHandler={this.somethingWentWrongCloseHandler}
        />
      );
    }
    return (
      <main className={classes.Profile}>
        <div className={classes.Profile_Container}>
          <Link
            to="/"
            style={{
              justifySelf: "start",
              marginTop: "2rem",
              marginLeft: "2rem"
            }}
            className={`${utilClasses.Link}`}
          >
            &larr; Go back
          </Link>
          <header className={classes.Profile_Header}>
            <h1 className={utilClasses.Secondary__Heading}>Add Experience</h1>
            <p
              className={utilClasses.Paragraph}
              style={{ justifySelf: "start", marginLeft: "1.5rem" }}
            >
              <br />* fileds are mark as required.
            </p>
          </header>
          <Form
            onSubmit={this.props.handleSubmit(this.experienceFormSubmitHandler)}
            className={classes.Profile_Form}
          >
            <Field
              component={renderInput}
              placeholder="* Company"
              name="company"
              error={errors.companyIsNotValid}
              label="* Company"
            />
            <Field
              component={renderInput}
              placeholder="* Title"
              name="title"
              error={errors.titleIsNotValid}
              label="* Title"
            />
            <Field
              component={renderInput}
              placeholder="Location"
              name="location"
              label="Location"
            />
            <Field
              component={renderInput}
              name="from"
              type="date"
              error={errors.fromIsNotValid}
              label="* From Date"
            />
            <Field
              component={renderInput}
              type="date"
              name="to"
              label="To Date"
              disabled={this.state.disabled ? true : false}
            />
            <Field
              onChange={this.checkBoxChangeHandler}
              component={renderInput}
              type="checkbox"
              name="current"
              label="Current Job"
              checked={this.state.current}
            />
            <Field
              component={TextAreaField}
              placeholder="Job Description"
              name="description"
              label="Job Description"
            />
            <button
              className={`${utilClasses.Button} ${classes.Profile_Form__Button}`}
              type="submit"
              disabled={this.props.pristine || this.props.submiting}
            >
              Submit Experience
            </button>
          </Form>
        </div>
      </main>
    );
  }
}
const mapStateToProps = state => {
  return {
    profileLoading: state.profileReducer.profileLoading,
    profileErrors: state.profileReducer.profileErrors,
    somethingWentWrong: state.profileReducer.somethingWentWrong
  };
};
const mapDispatchToProps = dispatch => {
  return {
    onAddExperience: (experienceData, history) =>
      dispatch(actions.addExperience(experienceData, history)),
    onSomethingWentWrongClose: () =>
      dispatch(actions.profileSomethingWentWrongCloseHandler())
  };
};

AddExperience = connect(
  mapStateToProps,
  mapDispatchToProps
)(AddExperience);

export default reduxForm({
  form: "addExperienceForm",
  validate: validateExperienceInput
})(AddExperience);
