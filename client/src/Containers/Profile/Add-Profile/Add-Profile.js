import React, { Component } from "react";
import classes from "../Profile.module.scss";
import utilClasses from "../../../Util/Util.module.scss";
import { connect } from "react-redux";
import SelectField from "../../../Components/Input/SelectField/SelectField";
import TextAreaField from "../../../Components/Input/TextAreaField/TextAreaField";
import * as actions from "../../../Store/Actions/IndexAction";
import Loader from "../../../Components/UI/Loader/Loader.js";
import SomethingWentWrong from "../../../HOC/ErrorHandler/SomethingWentWrong";
import { Field, Form, reduxForm } from "redux-form";
import renderInput from "../../../Components/Redux-Form/Renderers/RenderInput";
import validateProfileInput from "../../../Components/Redux-Form/Validators/AddProfileValidations";

// ADDIDING PROFILE INFORMATION FORM COMPONENT ...

class AddProfile extends Component {
  state = {
    displaySocialInputs: false
  };
  somethingWentWrongCloseHandler = () => {
    this.props.onSomethingWentWrongClose();
  };

  toggleDisplaySocialInputs = event => {
    event.preventDefault();
    this.setState(prevState => {
      return {
        displaySocialInputs: !prevState.displaySocialInputs
      };
    });
  };

  profileFormSubmitHandler = formValues => {
    this.props.onCreateProfile(formValues, this.props.history);
  };

  render() {
    let errors = { ...this.props.profileErrors };

    // SELECT OPTIONS FOR THE STATUS ...
    const options = [
      { label: "* Select Professional Status", value: 0 },
      { label: "Developer", value: "Developer" },
      { label: "Junior Developer", value: "Junior Developer" },
      { label: "Senior Developer", value: "Senior Developer" },
      { label: "Manager", value: "Manager" },
      { label: "Student or Learning", value: "Student or Learning" },
      { label: "Instructor or Teacher", value: "Instructor or Teacher" },
      { label: "Intern", value: "Intern" },
      { label: "Other", value: "Other" }
    ];
    let socialInputs;
    // TOGGLE SOCIAL INPUT BY TOGGLE BUTTON...
    if (this.state.displaySocialInputs) {
      socialInputs = (
        <div className={classes.Profile_Form__Social_Links}>
          <Field
            component={renderInput}
            placeholder="Fackebook Profile URL"
            label="Fackebook Profile URL"
            name="facebook"
            error={errors.facebookIsNotValid}
          />
          <Field
            component={renderInput}
            placeholder="Twitter Profile URL"
            label="Twitter Profile URL"
            name="twitter"
            error={errors.twitterIsNotValid}
          />
          <Field
            component={renderInput}
            placeholder="Instagram Profile URL"
            label="Instagram Profile URL"
            name="instagram"
            error={errors.instagramIsNotValid}
          />
          <Field
            component={renderInput}
            placeholder="YouTube Profile URL"
            label="YouTube Profile URL"
            name="youtube"
            error={errors.youtubeIsNotValid}
          />
          <Field
            component={renderInput}
            placeholder="Linkedin Profile URL"
            label="Linkedin Profile URL"
            name="linkedin"
            error={errors.linkedinIsNotValid}
          />
        </div>
      );
    }

    // IF PROFILE LOADING IS TRUE SHOW LOADER FUNTIONAL COMPONENT ...
    if (this.props.profileLoading) {
      return (
        <main className={utilClasses.Loader__Centered}>
          <Loader />
        </main>
      );
    }
    // IF SOMETHING BAD WENT WRONG SHOW MODAL WITH MESSAGE ...
    if (this.props.somethingWentWrong) {
      return (
        <SomethingWentWrong
          somethingWentWrong={this.props.somethingWentWrong}
          somethingWentWrongCloseHandler={this.somethingWentWrongCloseHandler}
        />
      );
    }
    return (
      <main className={classes.Profile}>
        <div className={classes.Profile_Container}>
          <header className={classes.Profile_Header}>
            <h1 className={utilClasses.Secondary__Heading}>
              Create Your Profile
            </h1>
            <p className={utilClasses.Paragraph}>
              Let's get some information to make your profile stand out.
            </p>
            <p
              className={utilClasses.Paragraph}
              style={{ justifySelf: "start", marginLeft: "1.5rem" }}
            >
              <br />* fileds are mark as required.
            </p>
          </header>
          <Form
            className={classes.Profile_Form}
            onSubmit={this.props.handleSubmit(this.profileFormSubmitHandler)}
          >
            <Field
              component={renderInput}
              placeholder="* Profile Handle"
              label="* Profile Handle"
              name="handle"
              error={errors.handleIsNotValid}
              info="A unique handle for your profile URL."
            />
            <Field
              component={SelectField}
              label="* Status"
              name="status"
              error={errors.statusIsNotValid}
              options={options}
              info="Give us idea about where you are at in career."
            />
            <Field
              component={renderInput}
              placeholder="Company"
              label="Company"
              name="company"
              info="Could be your own company or one you work for."
            />
            <Field
              placeholder="Website"
              component={renderInput}
              label="Website"
              name="website"
              error={errors.websiteIsNotValid}
              info="Could be your own website or your company website."
            />
            <Field
              placeholder="* Skills"
              label="* Skills"
              name="skills"
              component={renderInput}
              info="Use comma(,) to seperate the each skill (e.g.MongoDB,Express,React,Redux,Node,HTML5,CSS3,etc)."
            />
            <Field
              placeholder="Github User Name"
              label="Github User Name"
              name="githubUserName"
              component={renderInput}
              info="If you want your latest repos and Github user name,please include user name."
            />
            <Field
              component={TextAreaField}
              placeholder="Short Bio"
              label="Short Bio"
              name="bio"
              info="Tell us a little about yourself."
            />

            <div className={classes.Profile_Form__Social}>
              <button
                className={`${utilClasses.Button} ${classes.Profile_Form__Button}`}
                onClick={event => this.toggleDisplaySocialInputs(event)}
              >
                Add Social Network Links
              </button>
              <span className={classes.Profile_Form__Social_Optional}>
                Optional
              </span>
            </div>
            {socialInputs}
            <button
              type="submit"
              className={`${utilClasses.Button} ${classes.Profile_Form__Button}`}
            >
              Submit Profile
            </button>
          </Form>
        </div>
      </main>
    );
  }
}
const mapStateToProps = state => {
  return {
    profileLoading: state.profileReducer.profileLoading,
    profileErrors: state.profileReducer.profileErrors,
    somethingWentWrong: state.profileReducer.somethingWentWrong
  };
};
const mapDispatchToProps = dispatch => {
  return {
    onCreateProfile: (profileData, history) =>
      dispatch(actions.createProfile(profileData, history)),
    onSomethingWentWrongClose: () =>
      dispatch(actions.profileSomethingWentWrongCloseHandler())
  };
};
AddProfile = connect(
  mapStateToProps,
  mapDispatchToProps
)(AddProfile);

export default reduxForm({
  form: "profileForm",
  validate: validateProfileInput
})(AddProfile);
