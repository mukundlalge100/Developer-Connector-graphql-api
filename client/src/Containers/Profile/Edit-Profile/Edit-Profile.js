import React, { Component } from "react";
import classes from "../Profile.module.scss";
import utilClasses from "../../../Util/Util.module.scss";
import { connect } from "react-redux";
import SelectField from "../../../Components/Input/SelectField/SelectField";
import TextAreaField from "../../../Components/Input/TextAreaField/TextAreaField";
import * as actions from "../../../Store/Actions/IndexAction";
import Loader from "../../../Components/UI/Loader/Loader.js";
import { Link } from "react-router-dom";
import SomethingWentWrong from "../../../HOC/ErrorHandler/SomethingWentWrong";
import { reduxForm, Form, Field } from "redux-form";
import renderInput from "../../../Components/Redux-Form/Renderers/RenderInput";
import validateProfileInput from "../../../Components/Redux-Form/Validators/AddProfileValidations";

// EDIT PROFILE FORM COMPONENT...
class EditProfile extends Component {
  state = {
    displaySocialInputs: false
  };
  componentDidMount = () => {
    this.props.onGetProfile();
  };
  somethingWentWrongCloseHandler = () => {
    this.props.onSomethingWentWrongClose();
  };
  toggleDisplaySocialInputs = event => {
    event.preventDefault();
    this.setState(prevState => {
      return {
        displaySocialInputs: !prevState.displaySocialInputs
      };
    });
  };
  profileFormSubmitHandler = formValues => {
    const profileData = {
      handle: formValues.handle,
      company: formValues.company,
      bio: formValues.bio,
      website: formValues.website,
      location: formValues.location,
      status: formValues.status,
      skills: formValues.skills,
      githubUserName: formValues.githubUserName,
      facebook: formValues.facebook,
      youtube: formValues.youtube,
      twitter: formValues.twitter,
      linkedin: formValues.linkedin,
      instagram: formValues.instagram
    };
    this.props.onCreateProfile(profileData, this.props.history);
  };

  render() {
    let errors = this.props.profileErrors;
    // SELECT OPTIONS FOR THE STATUS ...
    const options = [
      { label: "* Select Professional Status", value: 0 },
      { label: "Developer", value: "Developer" },
      { label: "Junior Developer", value: "Junior Developer" },
      { label: "Senior Developer", value: "Senior Developer" },
      { label: "Manager", value: "Manager" },
      { label: "Student or Learning", value: "Student or Learning" },
      { label: "Instructor or Teacher", value: "Instructor or Teacher" },
      { label: "Intern", value: "Intern" },
      { label: "Other", value: "Other" }
    ];
    let socialInputs;
    if (this.state.displaySocialInputs) {
      socialInputs = (
        <div className={classes.Profile_Form__Social_Links}>
          <Field
            placeholder="Fackebook Profile URL"
            label="Fackebook Profile URL"
            name="facebook"
            component={renderInput}
            error={errors.facebookIsNotValid}
          />
          <Field
            component={renderInput}
            placeholder="Twitter Profile URL"
            label="Twitter Profile URL"
            name="twitter"
            error={errors.twitterIsNotValid}
          />
          <Field
            component={renderInput}
            placeholder="Instagram Profile URL"
            label="Instagram Profile URL"
            name="instagram"
            error={errors.instagramIsNotValid}
          />
          <Field
            component={renderInput}
            placeholder="YouTube Profile URL"
            label="YouTube Profile URL"
            name="youtube"
            error={errors.youtubeIsNotValid}
          />
          <Field
            placeholder="Linkedin Profile URL"
            label="Linkedin Profile URL"
            name="linkedin"
            component={renderInput}
            error={errors.linkedinIsNotValid}
          />
        </div>
      );
    }

    if (this.props.profileLoading) {
      return (
        <main className={utilClasses.Loader__Centered}>
          <Loader />
        </main>
      );
    }
    if (this.props.somethingWentWrong) {
      return (
        <SomethingWentWrong
          somethingWentWrong={this.props.somethingWentWrong}
          somethingWentWrongCloseHandler={this.somethingWentWrongCloseHandler}
        />
      );
    }
    return (
      <main className={classes.Profile}>
        <div className={classes.Profile_Container}>
          <Link
            to="/"
            style={{
              justifySelf: "start",
              marginTop: "2rem",
              marginLeft: "2rem"
            }}
            className={`${utilClasses.Link}`}
          >
            &larr; Go back
          </Link>
          <header className={classes.Profile_Header}>
            <h1 className={utilClasses.Secondary__Heading}>
              Edit Your Profile
            </h1>
            <p
              className={utilClasses.Paragraph}
              style={{ justifySelf: "start", marginLeft: "1.5rem" }}
            >
              <br />* fileds are mark as required.
            </p>
          </header>
          <Form
            onSubmit={this.props.handleSubmit(this.profileFormSubmitHandler)}
            className={classes.Profile_Form}
          >
            <Field
              component={renderInput}
              placeholder="* Profile Handle"
              label="* Profile Handle"
              name="handle"
              error={errors.handleIsNotValid}
              info="A unique handle for your profile URL."
            />
            <Field
              label="* Status"
              name="status"
              component={SelectField}
              error={errors.statusIsNotValid}
              options={options}
              info="Give us idea about where you are at in career."
            />
            <Field
              component={renderInput}
              placeholder="Company"
              label="Company"
              name="company"
              info="Could be your own company or one you work for."
            />
            <Field
              component={renderInput}
              placeholder="Website"
              label="Website"
              name="website"
              error={errors.websiteIsNotValid}
              info="Could be your own website or your company website."
            />
            <Field
              component={renderInput}
              placeholder="* Skills"
              label="* Skills"
              name="skills"
              error={errors.skillsAreNotValid}
              info="Use comma(,) to seperate the each skill (e.g.MongoDB,Express,React,Redux,Node,HTML5,CSS3,etc)."
            />
            <Field
              component={renderInput}
              placeholder="Github User Name"
              label="Github User Name"
              name="githubUserName"
              info="If you want your latest repos and Github user name,please include user name."
            />
            <Field
              component={TextAreaField}
              placeholder="Short Bio"
              label="Short Bio"
              name="bio"
              info="Tell us a little about yourself."
            />

            <div className={classes.Profile_Form__Social}>
              <button
                className={`${utilClasses.Button} ${classes.Profile_Form__Button}`}
                onClick={event => this.toggleDisplaySocialInputs(event)}
              >
                Add Social Network Links
              </button>
              <span className={classes.Profile_Form__Social_Optional}>
                Optional
              </span>
            </div>
            {socialInputs}
            <button
              className={`${utilClasses.Button} ${classes.Profile_Form__Button}`}
              type="submit"
            >
              Submit Profile
            </button>
          </Form>
        </div>
      </main>
    );
  }
}
const mapStateToProps = state => {
  let initialValues = null;
  if (state.profileReducer.profile) {
    initialValues = {
      handle: state.profileReducer.profile.handle,
      company: state.profileReducer.profile.company,
      bio: state.profileReducer.profile.bio,
      website: state.profileReducer.profile.website,
      location: state.profileReducer.profile.location,
      status: state.profileReducer.profile.status,
      skills: state.profileReducer.profile.skills,
      githubUserName: state.profileReducer.profile.githubUserName,
      facebook: state.profileReducer.profile.social.facebook,
      youtube: state.profileReducer.profile.social.youtube,
      twitter: state.profileReducer.profile.social.twitter,
      linkedin: state.profileReducer.profile.social.linkedin,
      instagram: state.profileReducer.profile.social.instagram
    };
  }

  return {
    initialValues: initialValues,
    profileLoading: state.profileReducer.profileLoading,
    profile: state.profileReducer.profile,
    profileErrors: state.profileReducer.profileErrors,
    somethingWentWrong: state.profileReducer.somethingWentWrong
  };
};
const mapDispatchToProps = dispatch => {
  return {
    onGetProfile: () => dispatch(actions.getProfile()),
    onCreateProfile: (profileData, history) =>
      dispatch(actions.createProfile(profileData, history)),
    onSomethingWentWrongClose: () =>
      dispatch(actions.profileSomethingWentWrongCloseHandler())
  };
};

EditProfile = reduxForm({
  form: "editProfileForm",
  enableReinitialize: true,
  validate: validateProfileInput
})(EditProfile);

EditProfile = connect(
  mapStateToProps,
  mapDispatchToProps
)(EditProfile);

export default EditProfile;
