module.exports = {
  getPostsQuery: `
    query {
      getPosts {
        _id
        text
        userName
        avatar
        date
        user {
          userName
          _id
        }
        likes{
          _id
          user {
            _id
          }
        }
        disLikes {
          user {
            _id
          }
          _id
        }
        comments {
          _id
          text
        }
      }
    }
  `,
  getPostQuery: `
    query {
      getPost{
        _id
        text
        date
        avatar
        userName
        user{
          userName
          avatar
          _id
        }
        comments {
          _id
          user{
            _id
            userName
          }
          text
          date
          userName
          avatar
        }
      }
    }
  `,
  deletePostQuery: `
    mutation {
      deletePost {
        message
        success
      }
    }
  `,
  deleteCommentQuery: `
    mutation {
      deleteComment{
        _id
        text
        user{
          _id
          userName
          avatar
        }
        userName
        avatar
        date
        comments{
          _id
          text
          userName
          avatar
          date
          user {
            _id
            userName
            avatar
          }
        }
      }
    }
  `,
  likePostByIdQuery: `
    mutation {
      likePostById{
        _id
        text
        userName
        avatar
        date
        user{
          _id
          userName
          avatar
        }
        likes{
          _id
          user {
            _id
          }
        }
        disLikes {
          _id
          user {
            _id
          }
        }
      }
    }
  `,
  unlikePostByIdQuery: `
    mutation {
      unlikePostById {
        _id
        text
        userName
        avatar
        date
        user{
          _id
          userName
          avatar
        }
        likes{
          _id
          user {
            _id
          }
        }
        disLikes {
          _id
          user {
            _id
          }
        }
      }
    }
  `,
  addPostQuery: `
    mutation{
      createPost{
        _id
        text
        userName
        avatar
        user {
          _id
          userName
          avatar
        }  
        likes {
          _id
        }
        disLikes{
          _id
        }
        comments{
          _id
        }
      }
    }
  `,
  addCommentQuery: `
     mutation {
      createComment{
        _id
        avatar  
        userName
        text
        likes{
          _id 
        }
        disLikes {
          _id
        }
        user{
          _id
        }
        date
        comments {
          _id
          text
          userName
          avatar
          date
          user {
            _id
          }
        }
      }
    }
  `
};
