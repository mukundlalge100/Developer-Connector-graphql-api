module.exports = {
  getProfilesQuery: `
    query {
      getProfiles {
        _id
        user {
          _id
          userName
          avatar
        }
        handle
        skills 
      }
    }
  `,
  getProfileQuery: `
    query {
      getProfile {
        _id 
        handle 
        status
        company
        bio
        githubUserName
        experience{
          _id
          title
          company 
          location
          from 
          to
          current
          description 
        }
        education {
          _id
          school
          degree
          fieldOfStudy
          from
          to
          current
          description 
        }
        skills
        user {
          _id
          userName 
          avatar
        }
        website
        social {
          youtube
          facebook
          linkedin
          twitter
          instagram
        }
      }
    }
  `,
  getProfileByHandleQuery: `
    query {
      getProfileByHandle {
        _id 
        handle 
        status
        company
        bio
        githubUserName
        experience{
          _id
          title
          company 
          location
          from 
          to
          current
          description 
        }
        education {
          _id
          school
          degree
          fieldOfStudy
          from
          to
          current
          description 
        }
        skills
        user {
          _id
          userName 
          avatar
        }
        website
        social {
          youtube
          facebook
          linkedin
          twitter
          instagram
        }
      }
    }
  `,
  getProfileByUserIdQuery: `
     query {
      getProfileByUserId {
        _id 
        handle 
        status
        company
        bio
        githubUserName
        experience{
          _id
          title
          company 
          location
          from 
          to
          current
          description 
        }
        education {
          _id
          school
          degree
          fieldOfStudy
          from
          to
          current
          description 
        }
        skills
        user {
          _id
          userName 
          avatar
        }
        website
        social {
          youtube
          facebook
          linkedin
          twitter
          instagram
        }
      }
    }
  `,
  deleteProfileAndAccountQuery: `
    mutation {
      deleteProfileAndAccount {
        message
      }
    }
  `,
  deleteExperienceQuery: `
    mutation {
      deleteExperience {
        handle
        experience {
          _id
          title
          company
          location
          from
          to
          current
          description
        }
        education {
          _id
          school
          degree
          fieldOfStudy
          from
          to
          current
          description
        }
      }
    }
  `,
  deleteEducationQuery: `
    mutation {
      deleteEducation {
        handle
        experience {
          _id
          title
          company
          location
          from
          to
          current
          description
        }
        education {
          _id
          school
          degree
          fieldOfStudy
          from
          to
          current
          description
        }
      }
    }
  `,
  createExperienceQuery: `
    mutation {
      createExperience {
        _id
        experience {
          _id
          title
          company
          location
          from
          to
          current
          description
        }
        education {
          _id
          school
          degree
          fieldOfStudy
          from
          to
          current
          description
        }
      }
    }
  `,
  createEducationQuery: `
    mutation {
      createEducation {
        _id
        experience {
          _id
          title
          company
          location
          from
          to
          current
          description
        }
        education {
          _id
          school
          degree
          fieldOfStudy
          from
          to
          current
          description
        }
      }
    }
  `,
  createProfileQuery: `
    mutation {
      createProfile {
        _id
        handle
        company
        website
        location
        status
        skills
        bio
        githubUserName
        social {
          youtube
          facebook
          linkedin
          twitter 
          instagram
        }
        experience {
          _id
          title
          company
          location
          from
          to
          current
          description
        }
        education {
          _id
          school
          degree
          fieldOfStudy
          from
          to
          current
          description
        }
      }
    }
  `
};
