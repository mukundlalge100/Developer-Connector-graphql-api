module.exports = {
  authSignUpQuery: `
    mutation {
      createUser{
        success
        message
      }
    }
  `,
  authLogInQuery: `
    query {
      logIn{
        token
      }
    }
  `,
  authResetPasswordQuery: `
    mutation {
      postResetPassword{
        success
        message
      }
    }
  `,
  authNewPasswordQuery: `
    mutation {
      postNewPassword{
        success
        message
      }
    }
  `,
  validateEmailQuery: `
    query {
      validateEmail {
        success
        message
      }
    }
  `
};
