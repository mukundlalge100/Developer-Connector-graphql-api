import { actionTypes } from "./ActionTypes";
import axios from "../../api/DevConnector";
import jwtDecode from "jwt-decode";
import { setAuthToken } from "../../Util/Util";
import * as actions from "./IndexAction";
import GraphQLQueries from "../../GraphQL/UserQueries";

const authSomethingWentWrong = somethingWentWrong => {
  return {
    type: actionTypes.AUTH_SOMETHING_WENT_WRONG,
    somethingWentWrong: somethingWentWrong
  };
};

export const authSomethingWentWrongCloseHandler = () => {
  return {
    type: actionTypes.AUTH_SOMETHING_WENT_WRONG_CLOSE
  };
};

const authSignUpStart = () => {
  return {
    type: actionTypes.AUTH_SIGNUP_START
  };
};
const authSignUpSuccess = () => {
  return {
    type: actionTypes.AUTH_SIGNUP_SUCCESS
  };
};
const authSignUpFail = errors => {
  return {
    type: actionTypes.AUTH_SIGNUP_FAIL,
    errors: errors
  };
};
export const authSignUp = (signUpData, history) => {
  return async dispatch => {
    dispatch(authSignUpStart());
    try {
      const response = await axios.post("/graphql", {
        query: GraphQLQueries.authSignUpQuery,
        signUpData
      });

      if (response.data.data.createUser.success) {
        dispatch(authSignUpSuccess());
        history.push("/login");
      }
    } catch (error) {
      if (error.response.data.errors && error.response !== undefined) {
        dispatch(authSignUpFail(error.response.data.errors[0].errors));
      } else {
        dispatch(authSomethingWentWrong(error.message));
      }
    }
  };
};

const authLogInStart = () => {
  return {
    type: actionTypes.AUTH_LOGIN_START
  };
};
const authLogInSuccess = user => {
  return {
    type: actionTypes.AUTH_LOGIN_SUCCESS,
    user
  };
};

const authLogInFail = errors => {
  return {
    type: actionTypes.AUTH_LOGIN_FAIL,
    errors
  };
};

export const authLogIn = (logInData, history) => {
  return async dispatch => {
    dispatch(authLogInStart());
    try {
      const response = await axios.post("/graphql", {
        query: GraphQLQueries.authLogInQuery,
        logInData
      });
      const { token } = response.data.data.logIn;
      localStorage.setItem("jwtToken", token);
      setAuthToken(token);
      const user = jwtDecode(token);
      dispatch(authLogInSuccess(user));
      history.push("/");
    } catch (error) {
      if (error.response.data.errors && error.response !== undefined) {
        dispatch(authLogInFail(error.response.data.errors[0].errors));
      } else {
        dispatch(authSomethingWentWrong(error.message));
      }
    }
  };
};
export const authLogOut = history => {
  setAuthToken(false);
  localStorage.removeItem("jwtToken");
  history.push("/");
  return {
    type: actionTypes.AUTH_LOGOUT
  };
};

export const checkExpiredTime = (expiredTime, history) => {
  return dispatch => {
    setTimeout(() => {
      dispatch(authLogOut(history));
    }, expiredTime * 1000);
  };
};

export const authCheckLogInState = history => {
  return dispatch => {
    if (localStorage.jwtToken) {
      setAuthToken(localStorage.jwtToken);
      const user = jwtDecode(localStorage.jwtToken);
      const currenTime = Date.now() / 1000;
      if (user.exp < currenTime) {
        dispatch(actions.clearCurrentProfile());
        dispatch(actions.clearCurrentPost());
        dispatch(authLogOut(history));
        return;
      }
      const totalExpTimeLeft = user.exp - currenTime;
      dispatch(authLogInSuccess(user));
      dispatch(checkExpiredTime(totalExpTimeLeft, history));
    }
  };
};

// RESET PASSWORD ACTIONS ...
export const authResetPasswordStart = () => {
  return {
    type: actionTypes.AUTH_RESET_PASSWORD_START
  };
};
export const authResetPasswordSuccess = () => {
  return {
    type: actionTypes.AUTH_RESET_PASSWORD_SUCCESS
  };
};
export const authResetPasswordFail = errors => {
  return {
    type: actionTypes.AUTH_RESET_PASSWORD_FAIL,
    errors
  };
};

export const authResetPassword = (resetPasswordData, history) => {
  return async dispatch => {
    dispatch(authResetPasswordStart());
    try {
      const response = await axios.post("/graphql", {
        query: GraphQLQueries.authResetPasswordQuery,
        resetPasswordData
      });
      if (response.data.data.postResetPassword.success) {
        dispatch(authResetPasswordSuccess());
        alert(response.data.data.postResetPassword.message);
        history.push("/login");
      }
    } catch (error) {
      if (error.response !== undefined) {
        const errors = error.response.data.errors[0].errors;
        if (errors) {
          dispatch(authResetPasswordFail(errors));
        } else {
          dispatch(authSomethingWentWrong(error.message));
        }
      }
    }
  };
};

// NEW PASSWORD ACTIONS ...
export const authNewPasswordStart = () => {
  return {
    type: actionTypes.AUTH_NEW_PASSWORD_START
  };
};
export const authNewPasswordSuccess = () => {
  return {
    type: actionTypes.AUTH_NEW_PASSWORD_SUCCESS
  };
};
export const authNewPasswordFail = errors => {
  return {
    type: actionTypes.AUTH_NEW_PASSWORD_FAIL,
    errors
  };
};

export const authNewPassword = (newPasswordData, token, history) => {
  return async dispatch => {
    dispatch(authNewPasswordStart());
    try {
      const response = await axios.post("/graphql", {
        query: GraphQLQueries.authNewPasswordQuery,
        newPasswordData,
        token
      });
      if (response.data.data.postNewPassword.success) {
        dispatch(authNewPasswordSuccess());
        alert(response.data.data.postNewPassword.message);
        history.push("/login");
      }
    } catch (error) {
      if (error.response !== undefined) {
        if (error.response.data.errors[0].errors) {
          dispatch(authNewPasswordFail(error.response.data.errors[0].errors));
        } else {
          dispatch(authSomethingWentWrong(error.message));
        }
      }
    }
  };
};
