import { actionTypes } from "./ActionTypes";
import axios from "../../api/DevConnector";
import GraphQLQuery from "../../GraphQL/PostQueries";

// CLEAR CURRENT POST AFTER LOGOUT ...
export const clearCurrentPost = () => {
  return {
    type: actionTypes.CLEAR_CURRENT_POST
  };
};
// POST SOMETHING WENT WRONG ACTION ...
const postSomethingWentWrong = somethingWentWrong => {
  return {
    type: actionTypes.POST_SOMETHING_WENT_WRONG,
    somethingWentWrong
  };
};
export const postSomethingWentWrongCloseHandler = () => {
  return {
    type: actionTypes.POST_SOMETHING_WENT_WRONG_CLOSE
  };
};

// ADD POST ACTIONS ...
const addPostStart = () => {
  return {
    type: actionTypes.ADD_POST_START
  };
};
const addPostSuccess = post => {
  return {
    type: actionTypes.ADD_POST_SUCCESS,
    post
  };
};
const addPostFail = errors => {
  return {
    type: actionTypes.ADD_POST_FAIL,
    errors
  };
};
export const addPost = postData => {
  return async dispatch => {
    try {
      dispatch(addPostStart());
      const response = await axios.post("/graphql", {
        query: GraphQLQuery.addPostQuery,
        postData
      });
      dispatch(addPostSuccess(response.data.data.createPost));
    } catch (error) {
      if (error.response.data.errors && error.response !== undefined) {
        dispatch(addPostFail(error.response.data.errors[0].errors));
      } else dispatch(postSomethingWentWrong(error.message));
    }
  };
};

// ADD COMMENT ACTIONS ...
const addCommentStart = () => {
  return {
    type: actionTypes.ADD_COMMENT_START
  };
};
const addCommentSuccess = post => {
  return {
    type: actionTypes.ADD_COMMENT_SUCCESS,
    post
  };
};
const addCommentFail = errors => {
  return {
    type: actionTypes.ADD_COMMENT_FAIL,
    errors
  };
};
export const addComment = (postId, commentData) => {
  return async dispatch => {
    try {
      dispatch(addCommentStart());
      const response = await axios.post("/graphql", {
        query: GraphQLQuery.addCommentQuery,
        postId,
        commentData
      });
      dispatch(addCommentSuccess(response.data.data.createComment));
    } catch (error) {
      if (error.response.data.errors && error.response !== undefined) {
        dispatch(addCommentFail(error.response.data.errors[0].errors));
      } else {
        dispatch(postSomethingWentWrong(error.message));
      }
    }
  };
};

// ADD COMMENT ACTIONS ...
const deleteCommentStart = () => {
  return {
    type: actionTypes.DELETE_COMMENT_START
  };
};
const deleteCommentSuccess = post => {
  return {
    type: actionTypes.DELETE_COMMENT_SUCCESS,
    post
  };
};
const deleteCommentFail = errors => {
  return {
    type: actionTypes.DELETE_COMMENT_FAIL,
    errors
  };
};
export const deleteComment = (commentId, postId) => {
  return async dispatch => {
    try {
      dispatch(deleteCommentStart());
      const response = await axios.post("/graphql", {
        query: GraphQLQuery.deleteCommentQuery,
        commentId,
        postId
      });
      dispatch(deleteCommentSuccess(response.data.data.deleteComment));
    } catch (error) {
      if (error.response.data.errors && error.response !== undefined) {
        dispatch(deleteCommentFail(error.response.data.errors[0].errrors));
      } else {
        dispatch(postSomethingWentWrong(error.message));
      }
    }
  };
};

// DELETE POST ACTIONS ...
const deletePostStart = () => {
  return {
    type: actionTypes.DELETE_POST_START
  };
};
const deletePostSuccess = postId => {
  return {
    type: actionTypes.DELETE_POST_SUCCESS,
    postId
  };
};
const deletePostFail = errors => {
  return {
    type: actionTypes.DELETE_POST_FAIL,
    errors
  };
};
export const deletePost = postId => {
  return async dispatch => {
    try {
      dispatch(deletePostStart());
      const response = await axios.post("/graphql", {
        query: GraphQLQuery.deletePostQuery,
        postId
      });
      if (response.data.data.deletePost.success) {
        dispatch(deletePostSuccess(postId));
      }
    } catch (error) {
      if (error.response.data.errors && error.response !== undefined) {
        dispatch(deletePostFail(error.response.data.errors[0].errors));
      } else {
        dispatch(postSomethingWentWrong(error.message));
      }
    }
  };
};

// GET  POSTS ACTIONS ...
const getPostsStart = () => {
  return {
    type: actionTypes.GET_POSTS_START
  };
};
const getPostsSuccess = posts => {
  return {
    type: actionTypes.GET_POSTS_SUCCESS,
    posts
  };
};
const getPostsFail = errors => {
  return {
    type: actionTypes.GET_POSTS_FAIL,
    errors
  };
};
export const getPosts = () => {
  return async dispatch => {
    try {
      dispatch(getPostsStart());
      const response = await axios.post("/graphql", {
        query: GraphQLQuery.getPostsQuery
      });
      dispatch(getPostsSuccess(response.data.data.getPosts));
    } catch (error) {
      if (error.response.data.errors && error.response !== undefined) {
        dispatch(getPostsFail(error.response.data.errors[0].errors));
      } else {
        dispatch(postSomethingWentWrong(error.message));
      }
    }
  };
};

//GET POST ACTIONS ...
const getPostStart = () => {
  return {
    type: actionTypes.GET_POST_START
  };
};
const getPostSuccess = post => {
  return {
    type: actionTypes.GET_POST_SUCCESS,
    post
  };
};
const getPostFail = errors => {
  return {
    type: actionTypes.GET_POST_FAIL,
    errors
  };
};
export const getPost = postId => {
  return async dispatch => {
    try {
      dispatch(getPostStart());
      const response = await axios.post("/graphql", {
        query: GraphQLQuery.getPostQuery,
        postId
      });
      dispatch(getPostSuccess(response.data.data.getPost));
    } catch (error) {
      if (error.response.data.errors && error.response !== undefined) {
        dispatch(getPostFail(error.response.data.errors[0].errors));
      } else {
        dispatch(postSomethingWentWrong(error.message));
      }
    }
  };
};

// LIKE POST ACTION ...
export const likePost = postId => {
  return async dispatch => {
    try {
      const response = await axios.post("/graphql", {
        query: GraphQLQuery.likePostByIdQuery,
        postId
      });
      if (response) {
        dispatch(getPosts());
      }
    } catch (error) {
      dispatch(postSomethingWentWrong(error.message));
    }
  };
};

// DISLIKE POST ACTION...
export const disLikePost = postId => {
  return async dispatch => {
    try {
      const response = await axios.post("/graphql", {
        query: GraphQLQuery.unlikePostByIdQuery,
        postId
      });
      if (response) {
        dispatch(getPosts());
      }
    } catch (error) {
      dispatch(postSomethingWentWrong(error.message));
    }
  };
};
