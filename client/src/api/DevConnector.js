import axios from "axios";

const DevConnector = axios.create({
  baseURL: "https://mysterious-mesa-99049.herokuapp.com"
  // baseURL: "http://localhost:4000"
});
export default DevConnector;
