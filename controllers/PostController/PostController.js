const validatePostInput = require("../../validation/PostValidations");
const Post = require("../../models/Post");
const User = require("../../models/User");

// GET ALL POSTS METHOD ...
exports.getPosts = async request => {
  try {
    if (!request.isAuth) {
      const error = new Error("Unauthorized User!");
      error.status = 401;
      throw error;
    }

    const posts = await Post.find()
      .populate("user", ["userName", "avatar"])
      .sort({ date: -1 });

    return posts;
  } catch (error) {
    return error;
  }
};
// GET  POST BY ITS ID METHOD ...
exports.getPost = async request => {
  const postId = request.body.postId;
  try {
    if (!request.isAuth) {
      const error = new Error("Unauthorized User!");
      error.status = 401;
      throw error;
    }

    const post = await Post.findById(postId).populate("user", [
      "userName",
      "avatar"
    ]);
    if (!post) {
      const error = new Error("No post is found !");
      error.errors = { postsNotFound: "No post is found!" };
      error.status = 404;
      throw error;
    }
    return post;
  } catch (error) {
    return error;
  }
};

// CREATE POST METHOD ...
exports.createPost = async request => {
  if (!request.isAuth) {
    const error = new Error("Unauthorized User!");
    error.status = 401;
    throw error;
  }
  // VALIDTE POST INPUT BEFORE CREATING IT...
  const { errors, isValid } = validatePostInput(request.body.postData);

  try {
    if (!isValid) {
      const error = new Error("Invalid inputs!");
      error.errors = errors;
      error.status = 422;
      throw error;
    }
    const newPost = new Post({
      text: request.body.postData.text,
      user: request.user._id,
      userName: request.body.postData.userName,
      avatar: request.body.postData.avatar
    });

    const post = await newPost.save();
    const user = await User.findById(request.user._id).select(
      "userName avatar"
    );

    post.user = user;
    return post;
  } catch (error) {
    console.log(error);
    return error;
  }
};
// DELETE POST BY ID METHOD ...
exports.deletePost = async request => {
  if (!request.isAuth) {
    const error = new Error("Unauthorized User!");
    error.status = 401;
    throw error;
  }
  const postId = request.body.postId;
  try {
    const post = await Post.findById(postId);

    // CHECK IF POST IS EXISTS OR NOT ...
    if (!post) {
      const error = new Error("No post found with this id!");
      error.errors = { postsNotFound: "No post found with this id!" };
      error.status = 404;
      throw error;
    }
    // CHECK IF USER IS AUTHORIZED OR NOT ...
    if (post.user.toString() !== request.user._id.toString()) {
      const error = new Error("Unauthorized user to delete post!");
      error.status = 401;
      throw error;
    }
    await post.remove();
    return { success: true, message: "Successfully deleted post!" };
  } catch (error) {
    return error;
  }
};

// LIKE POST BY ID METHOD ...
exports.likePostById = async request => {
  if (!request.isAuth) {
    const error = new Error("Unauthorized User!");
    error.status = 401;
    throw error;
  }
  const postId = request.body.postId;
  try {
    const post = await Post.findById(postId);

    // CHECK IF POST IS ALREADY EXISTS OR NOT ...
    if (!post) {
      const error = new Error("No post found with this id!");
      error.errors = { postsNotFound: "No post found with this id!" };
      error.status = 404;
      throw error;
    }

    // CHECK IF POST ALREADY LIKE OR NOT ...
    const alreadyLikePost =
      post.likes.filter(
        like => like.user.toString() === request.user._id.toString()
      ).length > 0;

    if (alreadyLikePost) {
      const likedIndex = post.likes.findIndex(
        like => like.user.toString() === request.user._id.toString()
      );
      post.likes.splice(likedIndex, 1);
      const updatedPost = await post.save();
      return updatedPost;
    }

    post.likes.unshift({ user: request.user._id });
    const updatedPost = await post.save();
    return updatedPost;
  } catch (error) {
    return error;
  }
};

// UNLIKE POST BY ID METHOD ...
exports.unlikePostById = async request => {
  if (!request.isAuth) {
    const error = new Error("Unauthorized User!");
    error.status = 401;
    throw error;
  }
  const postId = request.body.postId;
  try {
    const post = await Post.findById(postId);

    // CHECK IF POST IS ALREADY EXISTS OR NOT ...
    if (!post) {
      const error = new Error("No post found with this id!");
      error.errors = { postsNotFound: "No post found with this id!" };
      error.status = 404;
      throw error;
    }
    // CHECK IF POST ALREADY DISLIKE OR NOT ...
    const alreadyDisLikePost =
      post.disLikes.filter(
        disLike => disLike.user.toString() === request.user._id.toString()
      ).length > 0;

    // REMOVE USERID FROM DISLIKE ARRAY IF ALREADY DISLIKED...
    if (alreadyDisLikePost) {
      const unlikeIndex = post.disLikes.findIndex(
        disLike => disLike.user.toString() === request.user._id
      );
      post.disLikes.splice(unlikeIndex, 1);
      const updatedPost = await post.save();
      return updatedPost;
    }

    // ADD USERID INTO DISLIKE ARRAY IF NOT DISLIKED YET ...
    post.disLikes.unshift({ user: request.user._id });
    const updatedPost = await post.save();
    return updatedPost;
  } catch (error) {
    return error;
  }
};

//  POST comment BY ID METHOD ...
exports.postComment = async request => {
  if (!request.isAuth) {
    const error = new Error("Unauthorized User!");
    error.status = 401;
    throw error;
  }
  // VALIDTE COMMENT INPUT BEFORE CREATING IT...
  const { errors, isValid } = validatePostInput(request.body.commentData);

  try {
    if (!isValid) {
      const error = new Error("Invalid inputs!");
      error.errors = errors;
      error.status = 422;
      throw error;
    }
    const post = await Post.findById(request.body.postId);
    const newComment = {
      text: request.body.commentData.text,
      user: request.user._id,
      userName: request.body.commentData.userName,
      avatar: request.body.commentData.avatar
    };
    post.comments.unshift(newComment);
    const updatedPost = await post.save();
    return updatedPost;
  } catch (error) {
    return error;
  }
};

// UNLIKE POST BY ID METHOD ...
exports.deleteComment = async request => {
  if (!request.isAuth) {
    const error = new Error("Unauthorized User!");
    error.status = 401;
    throw error;
  }
  const postId = request.body.postId;
  const commentId = request.body.commentId;
  try {
    const post = await Post.findById(postId);

    // CHECK IF POST IS ALREADY EXISTS OR NOT ...
    if (!post) {
      const error = new Error("No post found with this id!");
      error.errors = { noPostFound: "No post found with this id!" };
      error.status = 404;
      throw error;
    }
    // CHECK IF COMMENT IS EXIST OR NOT ...
    const commentExist =
      post.comments.filter(
        comment => comment._id.toString() === commentId.toString()
      ).length > 0;

    if (!commentExist) {
      const error = new Error("No comment is found with id!");
      error.errors = { noPostFound: "No comment is found with id!" };
      error.status = 404;
      throw error;
    }

    const deleteIndex = post.comments.findIndex(
      comment => comment._id.toString() === commentId.toString()
    );
    post.comments.splice(deleteIndex, 1);

    const updatedPost = await post.save();
    return updatedPost;
  } catch (error) {
    return error;
  }
};
