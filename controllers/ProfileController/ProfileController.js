const Profile = require("../../models/Profile");
const User = require("../../models/User");
const Post = require("../../models/Post");

const validateProfileInput = require("../../validation/ProfileValidations");
const validateExperienceInput = require("../../validation/ExperienceValidations");
const validateEducationInput = require("../../validation/EducationValidations");
const isEmpty = require("../../util/isEmpty");

// GET PROFILE INFORMATION ...
exports.getProfiles = async request => {
  try {
    if (!request.isAuth) {
      const error = new Error("Unauthorized User!");
      error.status = 401;
      throw error;
    }
    const profiles = await Profile.find()
      .sort("handle")
      .populate("user", ["userName", "avatar"]);

    if (!profiles) {
      const error = new Error("No profiles found!");

      error.errors = { noProfilesFound: "No profiles found!!" };
      error.status = 404;
      throw error;
    }
    return profiles;
  } catch (error) {
    return error;
  }
};

// GET PROFILE INFORMATION ...
exports.getProfile = async request => {
  try {
    if (!request.isAuth) {
      const error = new Error("Unauthorized User!");
      error.status = 401;
      throw error;
    }
    const profile = await Profile.findOne({
      user: request.user._id
    }).populate("user", ["userName", "avatar"]);
    if (!profile) {
      const error = new Error("No profile found with this user!");
      error.errors = { profileNotFound: "No profile found with this user!" };
      error.status = 404;
      throw error;
    }
    return profile;
  } catch (error) {
    return error;
  }
};

// GET PROFILE INFORMATION BY ITS HANDLE...
exports.getProfileByHandle = async request => {
  try {
    if (!request.isAuth) {
      const error = new Error("Unauthorized User!");
      error.status = 401;
      throw error;
    }
    const handle = request.body.handle;
    const profile = await Profile.findOne({
      handle: handle
    }).populate("user", ["userName", "avatar"]);

    if (!profile) {
      const error = new Error("No profile found with this handle!");
      error.errors = { profileNotFound: "No profile found with this handle!" };
      error.status = 404;
      throw error;
    }
    return profile;
  } catch (error) {
    return error;
  }
};

// GET PROFILE INFORMATION BY ITS USERID...
exports.getProfileByUserId = async request => {
  try {
    const userId = request.body.userId;
    const profile = await Profile.findOne({
      user: userId
    }).populate("user", ["userName", "avatar"]);

    if (!profile) {
      const error = new Error("No profile found with this userId!");
      error.errors = { profileNotFound: "No profile found with this userId!!" };
      error.status = 404;
      throw error;
    }
    return profile;
  } catch (error) {
    return error;
  }
};

// POST PROFILE ROUTE RESPONSIBLE FOR CREATING OR EDITING PROFILE ...
exports.postProfile = async request => {
  try {
    const { formValues } = request.body;

    if (!request.isAuth) {
      const error = new Error("Unauthorized User!");
      error.status = 401;
      throw error;
    }

    // CHECK VALIDATIONS BEFORE SAVING PROFILE ...
    const { errors, isValid } = validateProfileInput(formValues);
    if (!isValid) {
      const error = new Error("Invalid Inputs!");
      error.status = 422;
      error.errors = errors;
      throw error;
    }
    // GET ALL PROFILE FIELDS ...
    let profileFields = {};
    // REQUIRED FIELDS ...
    profileFields.user = request.user._id;
    profileFields.handle = formValues.handle;
    profileFields.status = formValues.status;

    // SPLIT USER INTO ARRAY...
    if (!isEmpty(formValues.skills)) {
      if (Array.isArray(formValues.skills)) {
        profileFields.skills = formValues.skills;
      } else {
        profileFields.skills = formValues.skills.split(",");
      }
    }

    // OPTIONAL FIELDS ...
    if (!isEmpty(formValues.company))
      profileFields.company = formValues.company;
    if (!isEmpty(formValues.website))
      profileFields.website = formValues.website;
    if (!isEmpty(formValues.location))
      profileFields.location = formValues.location;
    if (!isEmpty(formValues.bio)) profileFields.bio = formValues.bio;
    if (!isEmpty(formValues.githubUserName))
      profileFields.githubUserName = formValues.githubUserName;

    // SOCIAL LINKS...
    profileFields.social = {};
    if (!isEmpty(formValues.youtube))
      profileFields.social.youtube = formValues.youtube;
    if (!isEmpty(formValues.facebook))
      profileFields.social.facebook = formValues.facebook;
    if (!isEmpty(formValues.instagram))
      profileFields.social.instagram = formValues.instagram;
    if (!isEmpty(formValues.linkedin))
      profileFields.social.linkedin = formValues.linkedin;
    if (!isEmpty(formValues.twitter))
      profileFields.social.twitter = formValues.twitter;

    const profile = await Profile.findOne({ user: request.user._id });

    if (profile) {
      // UPDATE PROFILE ...
      const updatedProfile = await Profile.findOneAndUpdate(
        { user: request.user._id },
        { $set: profileFields },
        { new: true }
      );
      return updatedProfile;
    } else {
      // CREATE PROFILE ...
      // CHECK IF PROFILE HANDLER IS ALREADY EXISTS OR NOT ...
      const alreadyExistsProfile = await Profile.findOne({
        handle: profileFields.handle
      });

      if (alreadyExistsProfile) {
        const error = new Error("That handle is already exists");
        error.errors = { handleIsNotValid: "That handle is already exists!" };
        error.status = 422;
        return error;
      }
      const newProfile = await new Profile(profileFields).save();
      return newProfile;
    }
  } catch (error) {
    return error;
  }
};

// POST EXPERIENCE METHOD ...
exports.postExperience = async request => {
  if (!request.isAuth) {
    const error = new Error("Unauthorized User!");
    error.status = 401;
    throw error;
  }
  // CHECK VALIDATIONS BEFORE SAVING PROFILE EXPERIENCE...
  const { errors, isValid } = validateExperienceInput(request.body.formValues);

  if (!isValid) {
    const error = new Error("Invalid Inputs!");
    error.errors = errors;
    error.status = 404;
    throw error;
  }

  try {
    const profile = await Profile.findOne({ user: request.user._id });

    if (!profile) {
      const error = new Error("Profile is not found!");
      error.status = 401;
      error.errors = { profileIsNotFound: "Profile is not found!" };
      throw error;
    }

    // ADD EXPERIENCE TO ARRAY ...
    profile.experience.unshift(request.body.formValues);

    const updatedProfile = await profile.save();
    return updatedProfile;
  } catch (error) {
    return error;
  }
};

// POST EDUCATION METHOD ...
exports.postEducation = async request => {
  if (!request.isAuth) {
    const error = new Error("Unauthorized User!");
    error.status = 401;
    throw error;
  }
  // CHECK VALIDATIONS BEFORE SAVING PROFILE EDUCATION...
  const { errors, isValid } = validateEducationInput(request.body.formValues);

  if (!isValid) {
    const error = new Error("Invalid Inputs!");
    error.errors = errors;
    error.status = 404;
    throw error;
  }

  try {
    const profile = await Profile.findOne({ user: request.user._id });

    if (!profile) {
      const error = new Error("Profile is not found!");
      error.status = 401;
      error.errors = { profileIsNotFound: "Profile is not found!" };
      throw error;
    }
    // ADD EDUCATION TO ARRAY ...
    profile.education.unshift(request.body.formValues);

    const updatedProfile = await profile.save();
    return updatedProfile;
  } catch (error) {
    return error;
  }
};

// DELETE EDUCATION DETAILS BY ID METHOD ...
exports.deleteEducation = async request => {
  if (!request.isAuth) {
    const error = new Error("Unauthorized User!");
    error.status = 401;
    throw error;
  }
  const educationId = request.body.educationId;
  try {
    const profile = await Profile.findOne({ user: request.user._id });

    if (!profile) {
      const error = new Error("Profile not found!");
      error.errors = { profileIsNotFound: "Profile is not found!" };
      error.status = 404;
      throw error;
    }
    // DELETE EDUCATION DETAILS FROM ARRAY ...
    const updatedEducation = profile.education.filter(
      edu => edu._id.toString() !== educationId.toString()
    );
    profile.education = updatedEducation;
    const updatedProfile = await profile.save();
    return updatedProfile;
  } catch (error) {
    return error;
  }
};

// DELETE EXPERIENCE DETAILS BY ID METHOD ...
exports.deleteExperience = async request => {
  if (!request.isAuth) {
    const error = new Error("Unauthorized User!");
    error.status = 401;
    throw error;
  }
  const experienceId = request.body.experienceId;

  try {
    const profile = await Profile.findOne({ user: request.user._id });

    if (!profile) {
      const error = new Error("Profile not found!");
      error.errors = { profileIsNotFound: "Profile is not found!" };
      error.status = 404;
      throw error;
    }
    // DELETE EXPERIENCE DETAILS FROM ARRAY ...
    const updatedExperience = profile.experience.filter(
      exp => exp._id.toString() !== experienceId.toString()
    );
    profile.experience = updatedExperience;
    const updatedProfile = await profile.save();
    return updatedProfile;
  } catch (error) {
    return error;
  }
};

// DELETE PROFILE AND USER ACCOUNT BY ID METHOD ...
exports.deleteProfileAndAccount = async request => {
  try {
    if (!request.isAuth) {
      const error = new Error("Unauthorized User!");
      error.status = 401;
      throw error;
    }
    await Profile.findOneAndDelete({
      user: request.user._id
    });

    await Post.findByIdAndDelete({ user: request.user._id });

    await User.findByIdAndDelete({ _id: request.user._id });

    return { message: "User account and profile deleted successfully" };
  } catch (error) {
    return error;
  }
};
