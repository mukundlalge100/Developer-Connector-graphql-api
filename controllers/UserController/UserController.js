const User = require("../../models/User");
const bcrypt = require("bcryptjs");
const crypto = require("crypto");
const validateNewPasswordInput = require("../../validation/NewPasswordValidations");

const gravatar = require("gravatar");
const jwt = require("jsonwebtoken");
const validateUserSignUpInput = require("../../validation/SignUpValidations");
const validateUserLogInInput = require("../../validation/LogInValidations");

// SENDGRID NODEMAILER CONFIG...
const nodeMailer = require("nodemailer");
const sendGridTransport = require("nodemailer-sendgrid-transport");

try {
  var transportor = nodeMailer.createTransport(
    sendGridTransport({
      auth: {
        api_key: process.env.SENDGRID_API_KEY
      }
    })
  );
} catch (error) {
  console.log(error);
}

exports.validateEmail = async request => {
  const email = request.body.validateEmailData.email;
  try {
    const user = await User.findOne({ email }).select("_id");
    if (!user) {
      return {
        success: true,
        message: "Email is not taken!"
      };
    } else {
      return {
        success: false,
        message: "Email is already Taken ,please try different one!"
      };
    }
  } catch (error) {
    return error;
  }
};
exports.postSignUp = async request => {
  try {
    const { errors, isValid } = validateUserSignUpInput(
      request.body.signUpData
    );
    if (!isValid) {
      const error = new Error("Invalid Inputs!!");
      error.status = 422;
      error.errors = errors;
      throw error;
    }
    const email = request.body.signUpData.email;
    const userName = request.body.signUpData.userName;
    const password = request.body.signUpData.password;

    const user = await User.findOne({ email: email });
    if (user) {
      const error = new Error("Email is already Exists!");
      error.status = 400;
      error.errors = { emailIsAlreadyExist: "Email is already exists!" };
      throw error;
    }
    const bcryptPassword = await bcrypt.hash(password, 16);
    const avatar = await gravatar.url(email, {
      size: "200",
      rating: "pg",
      default: "mm"
    });

    const newUser = new User({
      userName: userName,
      email: email,
      password: bcryptPassword,
      avatar: avatar
    });

    await newUser.save();
    return { success: true, message: "SignUp Successfully!" };
  } catch (error) {
    return error;
  }
};
exports.postLogIn = async request => {
  try {
    const { errors, isValid } = validateUserLogInInput(request.body.logInData);
    if (!isValid) {
      const error = new Error("Invalid Inputs!");
      error.status = 422;
      error.errors = errors;
      throw error;
    }
    const password = request.body.logInData.password;
    const email = request.body.logInData.email;

    const user = await User.findOne({ email: email });
    if (!user) {
      const error = new Error(
        "User is not exists,Please enter valid email address."
      );
      error.status = 404;
      error.errors = {
        emailIsNotValid: "User is not exists,Please enter valid email address"
      };
      throw error;
    }

    const isMatch = await bcrypt.compare(password, user.password);

    if (!isMatch) {
      const error = new Error(
        "Password credentials are invalid.Enter valid credentials."
      );
      error.status = 422;
      error.errors = {
        passwordIsNotValid:
          "Password credentials are invalid.Enter valid credentials."
      };
      throw error;
    }

    // PAYLOAD FOR TOKEN ...
    const payload = {
      _id: user._id,
      userName: user.userName,
      avatar: user.avatar
    };
    // JWT TOKEN AFTER SIGN IN ...
    const token = jwt.sign(payload, process.env.SECRET_OR_KEY, {
      expiresIn: "2h"
    });
    return { token: `Bearer ${token}` };
  } catch (error) {
    return error;
  }
};

// POST REQUEST FOR RESSETING USER PASSWORD ...
exports.postResetPassword = async request => {
  // GENERATING TOKEN USING CRYPTO MODULE ...
  const token = crypto.randomBytes(32).toString("hex");
  const email = request.body.resetPasswordData.email;

  try {
    // IF USER IS ALREADY EXISTS,THEN REDIRECT USER TO RESET PASSWORD PAGE WITH ERROR MESSAGES ...
    const user = await User.findOne({ email: email });
    if (!user) {
      const error = new Error("Email is not exists,Please enter valid email!");
      error.errors = {
        emailIsNotExist: "Email is not exists,Please enter valid email!"
      };
      error.status = 404;
      throw error;
    }
    user.passwordResetToken = token;
    user.passwordResetTokenExpiration = Date.now() + 3600000;
    // SAVE TOKEN AND TOKEN EXPIRATION TIME AFTER USER REQUEST FOR RESETING PASSWORD...
    await user.save();

    // AFTER SAVING TOKEN SEND USER EMAIL LINK FOR CHANGING PASSWORD...
    transportor.sendMail({
      to: email,
      from: "mukundlalge100@gmail.com",
      subject: "Reset your password",
      html: `<h2>Request for resetting your password</h2>
        <p>Click here <a href = http://localhost:3000/reset-password/${token}>to set new password</a>.</p>`
    });
    // REDIRECT USER AFTER SAVING AND SENDING EMAIL TO USER...
    return {
      success: true,
      message: "Email is send to your email address for resetting password!"
    };
  } catch (error) {
    return error;
  }
};

// POST REQUEST FOR CHANGING USER PASSWORD...
exports.postNewPassword = async request => {
  const password = request.body.newPasswordData.password;

  const token = request.body.token;
  let resetUser;

  // CHECKING VALIDATION ALL USER INPUT FIELDS...
  const { errors, isValid } = validateNewPasswordInput(
    request.body.newPasswordData
  );
  if (!isValid) {
    const error = new Error("Invalid Inputs!!");
    error.status = 422;
    error.errors = errors;
    throw error;
  }

  try {
    // IF USER HAS VALID TOKEN OR NOT ...
    const user = await User.findOne({
      passwordResetToken: token,
      passwordResetTokenExpiration: {
        $gt: Date.now()
      }
    }).select("_id email");

    if (!user) {
      // IF TOKEN IS INVALID OR EXPIRED REDIRECT USER TO RESET PASSWORD PAGE...
      const error = new Error("Invalid token or token has expired!");
      error.errors = {
        invalidToken: "Invalid token or token has expired!"
      };
      error.status = 401;
      throw error;
    }
    // ENCRYPT PASSWORD BEFORE STORING IT INTO DATABASE...
    const bcryptPassword = await bcrypt.hash(password, 16);

    resetUser = user;
    resetUser.password = bcryptPassword;

    // REMOVING USER TOKEN FROM DATABASE AFTER SETTING NEW PASSWORD...
    resetUser.passwordResetToken = undefined;
    resetUser.passwordResetTokenExpiration = undefined;
    await resetUser.save();

    // AFTER SUCCESSFULLY RESETTING NEW PASSWORD, SEND EMAIL TO USER FOR SUCCESS MESSAGE ...
    transportor.sendMail({
      to: user.email,
      from: "mukundlalge100@gmail.com",
      subject: "Congratulations,password reset successfully!",
      html: "<h1>You have successfully change your password!</h1>"
    });

    // REDIRECT USER AFTER SETTING NEW PASSWORD ...
    return {
      success: true,
      message: "Password reset successfully!"
    };
  } catch (error) {
    return error;
  }
};
